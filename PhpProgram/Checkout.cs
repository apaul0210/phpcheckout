﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using GitSharp.Core;
using System.IO;

namespace PhpProgram
{
    class Checkout
    {
        private String[] args;
        private System.Diagnostics.Process process;
        public void CheckoutVersion()
        {
            var client = new System.Net.WebClient();
            //client.DownloadFileAsync("");
        }

        public void ExecuteShell()
        {
            try
            {
                var cmd = "cmd.exe";
                var process = new System.Diagnostics.ProcessStartInfo(cmd);
                process.FileName = String.Format("{0}/{1}", System.IO.Directory.GetCurrentDirectory(), "gitClone.bat");
                process.UseShellExecute = true;
                System.Diagnostics.Process.Start(process);
                process.CreateNoWindow = true;

                var ExecuteFile = new ProcessStartInfo();
                ExecuteFile.FileName = "gitClone.bat";
                ExecuteFile.UseShellExecute = false;
                System.Diagnostics.Process.Start(ExecuteFile);
                process.RedirectStandardOutput = true;
                ExecuteFile.CreateNoWindow = true;
                ReadUserInput();
            }
            catch (Exception exception)
            {
                Console.WriteLine(String.Format("Failed to initiate process {0}", exception));
            }
        }
        public void ReadUserInput()
        {
            String command;
            command = Console.ReadLine();
            switch (command)
            {
                case "/Yes":
                    Console.WriteLine("Processing Checkout");
                    PutFolderOnCDrive();
                    break;
                case "/Y":
                    Console.WriteLine("Processing Checkout");
                    PutFolderOnCDrive();
                    break;
                case "/y":
                    Console.WriteLine("Processing Checkout");
                    PutFolderOnCDrive();
                    break;
                case "/No":
                    Console.WriteLine("Processing Cancelled");
                    process.Kill();
                    break;
                case "/N":
                    Console.WriteLine("Processing Cancelled");
                    process.Kill();
                    break;
                case "/n":
                    Console.WriteLine("Processing Cancelled");
                    process.Kill();
                    break;
            }
        }


        public void PutFolderOnCDrive()
        {
            var CDrive = String.Format("{0}{1}", Path.GetPathRoot(Environment.CurrentDirectory), "php-src");
            if (Directory.Exists(CDrive))
            Console.WriteLine("A folder with the same name already exists do you want to continue with checkout and moving of files? \n");
            ExecuteShell();
        }

        //public void checkoutfromGit(string [] args)
        //{
        //    try
        //    {
        //        for (int i = 0; i < args.Length; i++)
        //        {
        //            var temp = new Checkout();
        //            //Thread obj = new Thread();
        //            //obj.IsBackground = true;
        //            //obj.Priority = ThreadPriority.AboveNormal;
        //            //obj.Start(args);
        //            //Console.WriteLine(String.Format("{0}", args[i]));
        //        }
        //    }
        //    catch (Exception exception)
        //    {
        //        Console.WriteLine(String.Format("The following was not checkout out successfully {0}{1}", exception.StackTrace, exception.Message));
        //    }
        //}
    }
}
